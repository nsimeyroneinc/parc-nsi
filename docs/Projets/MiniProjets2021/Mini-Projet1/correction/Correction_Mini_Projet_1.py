#!/usr/bin/env python
# coding: utf-8

# # Préambule 

# <div class = "alert alert-warning">
# Ce fichier  est un notebook Python.
# 
# Il comporte deux types de cellules :
# 
# * les cellules d'édition dans lesquelles vous pouvez saisir du texte éventuellement enrichi de mises en formes ou de liens hypertextes avec la syntaxe du langage HTML simplifié qui s'appelle Markdown. Voir http://daringfireball.net/projects/markdown/ pour la syntaxe de Markdown.
# 
# * les cellules de code où l'on peut saisir du code Python3 puis le faire exécuter avec la combinaison de touches `CTRL + RETURN`
# 
# Une cellule peut être éditée  de deux façons différentes :
# 
# * en mode _commande_ lorsqu'on clique sur sa marge gauche qui est surlignée alors en bleu, on peut alors  :
# 
#     - changer le type de la cellule en appuyant sur `m` pour passer en cellule Markdown ou sur `y` pour passer en cellule de code
#     
#     - insérer une cellule juste au-dessus en appuyant sur `a`
#     
#     - insérer une cellule juste en-dessous en appuyant sur `b`
#     
#     - couper la cellule en appuyant sur `x` etc ...
#     
# * en mode _édition_ lorsqu'on clique sur l'intérieur de la cellule.
# 
# L'aide complète sur les raccourcis claviers est accessible depuis le bouton `Help` dans la barre d'outils ci-dessus.
# <div>

# # Sujet 1

# ## Exercice 1
# 
# 
# 1. Écrire une fonction `max2(a, b)` qui renvoie le maximum de deux entiers sans utiliser la fonction `built-in`  `max`.
# 
# ~~~python
# >>> max2(10, 8)
# 10
# ~~~
# 
# 2. Écrire une fonction `max3(a, b, c)` qui renvoie le maximum de trois entiers qui renvoie le maximum de deux entiers sans utiliser la fonction `built-in`  `max`.
# 
# 
# ~~~python
# >>> max3(10, 8, 10)
# 10
# ~~~
# 

# ### Correction de l'exercice 1

# In[3]:


def max2(a, b):
    """Signature : max2(a:int, b:int)-> int
    Postcondition : renvoie le maximum des arguments a et b"""
    if a >= b:
        return a
    else:
        return b
    
#Tests unitaires pour max2
assert max2(10, 10) == 10
assert max2(10, 0) == 10
assert max2(0, 10) == 10
assert max2(-2, -1) == -1
    
def max3(a, b, c):
    """Signature : max3(a:int, b:int, c:int)-> int
    Postcondition : renvoie le maximum des arguments a, b et c"""
    if a >= b:
        if a >= c:
            return a
        else:
            return c
    else:
        if b >= c:
            return b
        else:
            return c
        
#Tests unitaires pour max3
assert max3(10, 10, 10) == 10
assert max3(3,2,1) == 3
assert max3(2,3,1) == 3
assert max3(2,1,3) == 3
assert max3(-2,-1,-3) == -1
assert max3(2,1,2) == 2


# ## Exercice 2

# Écrire un programme qui demande d'abord  à l'utilisateur un nombre entier de notes tant  que le nombre saisi n'est pas strictement positif. 
# 
# Ensuite le programme demande à l'utilisateur de saisir chaque note et affiche en sortie la note maximale et le nombre de fois où elle est atteinte. 
# 
# Attention ! Interdiction d'utiliser les listes/tableaux Python.
# 
# Voici une trace d'exécution du programme :
# 
# ~~~
# Nombre de notes ? -2
# 
# Saisir un nombre > 0, nombre de notes ? -1
# 
# Saisir un nombre > 0, nombre de notes ? 3
# 
# Nouvelle note ? 10
# 
# Nouvelle note ? 8
# 
# Nouvelle note ? 10
# Note maximale :  10.0  atteinte  1 fois
# ~~~

# ### Correction de l'exercice 2

# In[1]:


nb_notes = int(input('Nombre de notes ? '))
while nb_notes <= 0:
    nb_notes = int(input('Saisir un nombre > 0, nombre de notes ? '))
note_maxi = 0
compteur_maxi = 0
for k in range(nb_notes):
    note = float(input('Nouvelle note ? '))
    if note > note_maxi:
        note_maxi = note
        compteur_maxi = 1
    elif note == note_maxi:
        compteur_maxi = compteur_maxi + 1
print("Note maximale : ", note_maxi, " atteinte ", compteur_maxi, "fois")


# # Sujet 2

# ## Exercice 1

# 1. Écrire une fonction `bac(moyenne)` qui prend en argument une moyenne de type `float` entre 0 et 20, vérifie si la moyenne est entre 0 et 20 avec une instruction `assert` et renvoie  :
#     * `True` si `moyenne >= 10` 
#     * `False` si `moyenne < 10`
#     
# 2. Écrire une fonction `mention(moyenne)` qui prend en argument une moyenne de type `float` entre 0 et 20 et qui renvoie une chaîne de caractère :
#     * `"recalé"` si  0 $\leqslant$ `note` $<$ 8
#     * `"second groupe"` si  8 $\leqslant$ `note` $<$ 10
#     * `"reçu"` si 10 $\leqslant$ `note` $<$ 12
#     * `"assez bien"` si 12 $\leqslant$ `note` $<$ 14
#     * `"bien"` si 14 $\leqslant$ `note` $<$ 16
#     * `"très bien"` si 16 $\leqslant$ `note` $\leqslant$ 20
#     * `"valeur incohérente"` sinon

# ### Correction de l'exercice 1

# In[10]:


def bac(moyenne):
    """Signature bac(moyenne:float)->bool
    Psotcondition : renvoie True si moyenne >= 10 et False sinon"""
    assert moyenne >= 0 and moyenne <= 20, "valeur incohérente"
    return moyenne >= 10


# In[11]:


bac(-10)


# In[20]:


def mention(moyenne):
    """Signature bac(moyenne:float)->str"""
    if moyenne < 0 or moyenne > 20:
        return "valeur incohérente"
    elif moyenne < 8:
        return "recalé"
    elif moyenne < 10:
        return "second groupe"
    elif moyenne < 12:
        return "reçu"
    elif moyenne < 14:
        return "assez bien"
    elif moyenne < 16:
        return "bien"
    elif moyenne <= 20:
        return "très bien"       


# In[21]:


#Tests unitaires
assert mention(-1) == "valeur incohérente"
assert mention(21)  == "valeur incohérente"
assert mention(0) == "recalé"
assert mention(2) == "recalé"
assert mention(8) == "second groupe"
assert mention(9) == "second groupe"
assert mention(10) == "reçu"
assert mention(11) == "reçu"
assert mention(12) == "assez bien"
assert mention(13) == "assez bien"
assert mention(14) == "bien"
assert mention(15) == "bien"
assert mention(16) == "très bien"
assert mention(18) == "très bien"
assert mention(20) == "très bien"


# ## Exercice 2

# Écrire un programme qui demande d'abord  à l'utilisateur un nombre entier de notes tant  que le nombre saisi n'est pas strictement positif. 
# 
# Ensuite le programme demande à l'utilisateur de saisir chaque note et affiche en sortie la moyenne des notes saisies, arrondie au centième.
# 
# Attention ! Interdiction d'utiliser les listes/tableaux Python.
# 
# Voici une trace d'exécution du programme :
# 
# ~~~
# Nombre de notes ? -2
# 
# Saisir un nombre > 0, nombre de notes ? 3
# 
# Nouvelle note ? 10
# 
# Nouvelle note ? 8
# 
# Nouvelle note ? 5
# Moyenne :  7.67
# ~~~
# 
# 

# ### Correction de l'exercice 2

# In[24]:


nb_notes = int(input('Nombre de notes ? '))
while nb_notes <= 0:
    nb_notes = int(input('Saisir un nombre > 0, nombre de notes ? '))
cumul = 0
for k in range(nb_notes):
    note = float(input('Nouvelle note ? '))
    cumul = cumul + note
print("Moyenne : ", round(cumul / nb_notes, 2))


# # Sujet 3

# ## Exercice 1

# Écrire une fonction prenant en paramètres les bornes de deux intervalles fermés $[a~;~b]$ et $[c~;~d]$ de $\mathbb{R}$ et qui retourne `True` si leur intersection est non vide et `False` sinon. On donne ci-dessous la spécification de cette fonction.
# 
# 
# ~~~python
# def intersection_intervalle(a,b,c,d):
# 	"""
# 	Signature : intersection_intervalle(a:int,b:int,c:int,d:int)->bool
# 	Détermine si les intervalles [a,b] et [c,d] ont une intersection non vide
# 	"""
# ~~~
# 
# Si tout se passe bien, on aura ça :
# 
# ~~~python
# >>> intersection_intervalle(843, 844, 841, 842)
# False
# >>> intersection_intervalle(840, 841, 841, 842)
# True
# ~~~
# 
# Mettre entre triples guillemets, les tests effectués en essayant de couvrir tous les cas possibles.

# ### Correction de l'exercice 1

# In[26]:


def intersection_intervalle(a,b,c,d):
    """
    Signature : intersection_intervalle(a:float,b:float,c:float,d:float)->bool
    Précondition : vérifier si les intervalles [a,b] et [c,d] sont valides avec un assert
    Postcondition : Détermine si les intervalles [a,b] et [c,d] ont une intersection non vide
    """
    assert a <= b and c <= d
    return not(b < c or d < a)

# Test unitaires
assert not intersection_intervalle(843, 844, 841, 842)
assert intersection_intervalle(840, 841, 841, 842)


# In[27]:


def intersection_rectangle(x1min,x1max,y1min,y1max,x2min,x2max,y2min,y2max):
    """
    Signature : intersection_rectangle(x1min:float,x1max,y1min,y1max,x2min,x2max,y2min,y2max)->bool
    Postcondition : Détermine si les rectangles (x1min,x1max,y1min,y1max) et (x2min,x2max,y2min,y2max)
    ont une intersection
    """
    return intersection_intervalle(x1min,x1max,x2min,x2max) and intersection_intervalle(y1min,y1max,y2min,y2max)


# ## Exercice 2

# In[1]:


from turtle import *


try:
    pas = 10
    for k in range(18):
        forward(pas)
        pas = pas + 5
        left(60)
    exitonclick()
except Terminator:
    pass


# ![spirale](spirale.png)

# In[4]:


from turtle import *


try:
    for i in range(8):
        for j in range(2):
            forward(50)
            left(90)
        forward(50)
        right(90 + 45)
    exitonclick()
except Terminator:
    pass


# ![pétales](petales.png)

# # Sujet 4  

# In[87]:


def triangle_rectangle_gauche(cote, caractere):
    assert cote > 0 #précondition
    for k in range(1, cote + 1):
        print(k * caractere)


# In[88]:


triangle_rectangle_gauche(4, '@')


# In[85]:


def carre(cote, caractere):
    assert cote > 0 #précondition
    for k in range(1, cote + 1):
        print(cote * caractere)


# In[86]:


carre(5, '@')


# In[81]:


def triangle_rectangle_droit(cote, caractere):
    assert cote > 0 #précondition
    for k in range(1, cote+ 1):
        print((cote - k) * ' ' + k * caractere)


# In[82]:


triangle_rectangle_droit(5, '@')


# In[89]:


def triangle_isocele(base, caractere):
    for k in range(1, base + 1):
        print((base - k) * ' ' + (2 * k  - 1 )* caractere)


# In[90]:


triangle_isocele(5, '@')


# In[91]:


def sapin_noel(base, tronc):
    triangle_isocele(base, '*')
    for k in range(tronc):
        print(' ' * (base - 1) + '|')


# In[92]:


sapin_noel(8,3)


# In[72]:


def losange(diagonale, caractere):
    assert diagonale % 2 == 1
    demi_diag = diagonale // 2
    for i in range(0,  diagonale):
        if i <= demi_diag:
            print(' ' * (demi_diag - i) + caractere * (i * 2 + 1))
        else:
            print(' ' * (i - demi_diag) + caractere * ((demi_diag - (i - demi_diag)) * 2 + 1))


# In[75]:


losange(11, '@')


# # Sujet 5 Pierre/Feuille/Ciseau (exo 41 p. 47)

# In[8]:


PIERRE = 0
PAPIER = 1
CISEAU = 2

def point(choixA, choixB):
    """Paramètres point(choixA:int, choixB:int)->int
    Postcondition : renvoie 1 si A gagne, -1 si B gagne
    et 0 sinon lors d'une partie de Pierre/Papier/Ciseau"""
    #préconditions
    assert choixA in [PIERRE, PAPIER, CISEAU]
    assert choixB in [PIERRE, PAPIER, CISEAU]
    if choixA == choixB:
        return 0
    elif (choixA == PIERRE and choixB == CISEAU):
        return 1
    elif (choixA == CISEAU and choixB == PAPIER):
        return 1
    elif (choixA == PAPIER and choixB == PIERRE):
        return 1
    else:
        return -1


# In[9]:


import random

def tour():
    """Simule une partie de Pierre/Feuille/Ciseau
    Renvoie un nombre positif si A gagne ou négatif si B gagne"""
    mise = 1
    p = point(random.randint(0,2), random.randint(0,2))
    while abs(p) != 1:
        mise = mise + 1
        p = point(random.randint(0,2), random.randint(0,2))
    return p * mise


# In[12]:


def partie50():
    """initialise les scores et joue 50 tours 
    en mettant à jour les scores et en les affichant."""
    scoreA = 0
    scoreB = 0
    for _ in range(50):
        t = tour()
        if t > 0:
            scoreA = scoreA + t
        else:
            scoreB = scoreB - t
        print(f"Score de A : {scoreA} et score de B : {scoreB}")


# In[13]:


partie50()


# # Sujet 6 : graphe de la fonction sinus (exo 28 p.44)

# __Question a)__

# In[8]:


from math import sin, pi
pas = pi / 14
for k in range(15):
    print(sin(pas * k))


# __Questions b) et c)__

# In[9]:


from math import sin, pi
pas = pi / 14
for k in range(15):
    print(round(30 * sin(pas * k)) * '=')


# __Question d)__

# In[12]:


from math import sin, pi 
pas = 2 * pi / 40
#affichage en découpant en subdivisions de 2 * pi / 40
#on a donc (40 + 1) batons (ceux de 0, pi, 2 * pi sont de hauteur nulle)
for k in range(41):  
    val = round(30 * sin(pas * k))
    if val > 0:
        print(30 * ' ' + val * '=')
    else:
        print((30 - abs(val)) * ' ' + abs(val) * '=')


# __Question e)__

# In[14]:


from math import sin, pi 
pas = 2 * pi / 40
#affichage en découpant en subdivisions de 2 * pi / 40
#on a donc (40 + 1) batons (ceux de 0, pi, 2 * pi sont de hauteur nulle)
for k in range(41):  
    val = round(30 * sin(pas * k))
    if val > 0:
        print((30 + val - 1) * ' ' + '*')
    else:
        print((30 - abs(val)) * ' ' + '*')


# # Sujet 7 : rendu de monnaie 'exo 34 p. 46)

# ## Question a) : rendu de monnaie glouton, on rend d'abord les plus grosses pièces

# In[17]:



nb_50euros_a_rendre = 0       # nombre de billets de 50€
nb_20euros_a_rendre = 0       # nombre de billets de 20€
nb_10euros_a_rendre = 0       # nombre de billets de 10€
nb_5euros_a_rendre = 0        # nombre de billets de 5€
nb_1euro_a_rendre = 0         # nombre de pièces de 1€

prix= 0
while prix <= 0 :
    prix = int(input("Prix à payer: "))        # prix à payer
   
recette = int(input("Montant déjà payé: "))   # montant reçu du client
while recette < prix :                          # vérifie que la somme donné est suffisante
       print ("Il manque", prix - recette,"euros")
       recette  = int(input("Payer le montant demandé:"))   # le client ressaisit une somme d'argent 

a_rendre = recette - prix       # calcul de la somme qu'on doit rendre au client (ce qu'il a payé - ce qu'il devait payer)

if a_rendre ==0 :            # si il n'y a rien à rendre, sortir de la boucle
       print ("\naucun rendu de monnaie\n")
else:   
    while a_rendre >= 50:
        nb_50euros_a_rendre = nb_50euros_a_rendre + 1
        a_rendre = a_rendre - 50
    while a_rendre >= 20:
        nb_20euros_a_rendre = nb_20euros_a_rendre + 1
        a_rendre = a_rendre - 20
    while a_rendre >= 10:
        nb_10euros_a_rendre = nb_10euros_a_rendre + 1
        a_rendre = a_rendre - 10
    while a_rendre >= 5:
        nb_5euros_a_rendre = nb_5euros_a_rendre + 1
        a_rendre = a_rendre - 5
    while a_rendre >= 1:
        nb_1euro_a_rendre = nb_1euro_a_rendre + 1
        a_rendre = a_rendre - 1
    # j'affiche le nombre de billets et de pièces de chaque valeur à rendre
    print ("nb de billets de 50€ à rendre: ", nb_50euros_a_rendre)
    print ("nb de billets de 20€ à rendre: ", nb_20euros_a_rendre)
    print ("nb de billets de 10€ à rendre: ", nb_10euros_a_rendre)
    print ("nb de billets de 5€ à rendre: ", nb_5euros_a_rendre)
    print ("nb de pièces de 1€ à rendre: ", nb_1euro_a_rendre)


# La multiplication de variables qui ont la même fonctionnalité va motiver l'introduction d'un nouveau type de données, le tableau.

# In[19]:



billet = [50, 20, 10, 5, 1]
billet_a_rendre = [0,0,0,0,0]
# billet_a_rendre[0]  ->  nombre de billets de 50€
# billet_a_rendre[1]  ->  nombre de billets de 20€
# billet_a_rendre[2]  ->  nombre de billets de 10€
# billet_a_rendre[3]  ->  nombre de billets de 5€
# billet_a_rendre[4]  ->  nombre de billets de 1€


prix= 0
while prix <= 0 :
    prix = int(input("Prix à payer: "))        # prix à payer
   
recette = int(input("Montant déjà payé: "))   # montant reçu du client
while recette < prix :                          # vérifie que la somme donné est suffisante
       print ("Il manque", prix - recette,"euros")
       recette  = int(input("Payer le montant demandé:"))   # le client ressaisit une somme d'argent 

a_rendre = recette - prix       # calcul de la somme qu'on doit rendre au client (ce qu'il a payé - ce qu'il devait payer)

if a_rendre ==0 :            # si il n'y a rien à rendre, sortir de la boucle
       print ("\naucun rendu de monnaie\n")
else:   
    for k in range(len(billet)):        
        while a_rendre >= billet[k]:
            billet_a_rendre[k] = billet_a_rendre[k] + 1
            a_rendre = a_rendre - billet[k]
     # j'affiche le nombre de billets et de pièces de chaque valeur à rendre
    for k in range(len(billet)): 
        print ("nb de billets de ", billet[k], "€  à rendre: ",billet_a_rendre[k])


# ## Question b)  :  contrainte supplémentaire : caisse limitée

# In[23]:


caisse = [4, 5, 12, 4, 15]
billet = [50, 20, 10, 5, 1]
billet_a_rendre = [0,0,0,0,0]
# billet_a_rendre[0]  ->  nombre de billets de 50€
# billet_a_rendre[1]  ->  nombre de billets de 20€
# billet_a_rendre[2]  ->  nombre de billets de 10€
# billet_a_rendre[3]  ->  nombre de billets de 5€
# billet_a_rendre[4]  ->  nombre de billets de 1€


prix= 0
while prix <= 0 :
    prix = int(input("Prix à payer: "))        # prix à payer
   
recette = int(input("Montant déjà payé: "))   # montant reçu du client
while recette < prix :                          # vérifie que la somme donné est suffisante
       print ("Il manque", prix - recette,"euros")
       recette  = int(input("Payer le montant demandé:"))   # le client ressaisit une somme d'argent 

a_rendre = recette - prix       # calcul de la somme qu'on doit rendre au client (ce qu'il a payé - ce qu'il devait payer)

if a_rendre ==0 :            # si il n'y a rien à rendre, sortir de la boucle
       print ("\naucun rendu de monnaie\n")
else:   
    for k in range(len(billet)):        
        while a_rendre >= billet[k] and caisse[k] > 0:
            billet_a_rendre[k] = billet_a_rendre[k] + 1
            caisse[k] -= 1
            a_rendre = a_rendre - billet[k]
     # j'affiche le nombre de billets et de pièces de chaque valeur à rendre
    for k in range(len(billet)): 
        print ("nb de billets de ", billet[k], "€  à rendre: ",billet_a_rendre[k])


# ## Question c)  :  pas assez de monnaie

# In[24]:


caisse = [4, 0, 0, 3, 15]
billet = [50, 20, 10, 5, 1]
billet_a_rendre = [0,0,0,0,0]
# billet_a_rendre[0]  ->  nombre de billets de 50€
# billet_a_rendre[1]  ->  nombre de billets de 20€
# billet_a_rendre[2]  ->  nombre de billets de 10€
# billet_a_rendre[3]  ->  nombre de billets de 5€
# billet_a_rendre[4]  ->  nombre de billets de 1€

prix= 0
while prix <= 0 :
    prix = int(input("Prix à payer: "))        # prix à payer
   
recette = int(input("Montant déjà payé: "))   # montant reçu du client
while recette < prix :                          # vérifie que la somme donné est suffisante
       print ("Il manque", prix - recette,"euros")
       recette  = int(input("Payer le montant demandé:"))   # le client ressaisit une somme d'argent 

a_rendre = recette - prix       # calcul de la somme qu'on doit rendre au client (ce qu'il a payé - ce qu'il devait payer)

if a_rendre ==0 :            # si il n'y a rien à rendre, sortir de la boucle
       print ("\naucun rendu de monnaie\n")
else:   
    for k in range(len(billet)):        
        while a_rendre >= billet[k] and caisse[k] > 0:
            billet_a_rendre[k] = billet_a_rendre[k] + 1
            caisse[k] -= 1
            a_rendre = a_rendre - billet[k]
     # j'affiche le nombre de billets et de pièces de chaque valeur à rendre
    if a_rendre > 0:
        print("Désolé je n'ai pas la monnaie")
    else:
        for k in range(len(billet)): 
            print ("nb de billets de ", billet[k], "€  à rendre: ",billet_a_rendre[k])


# ## Question d) : on encapsule dans une fonction

# In[26]:


def rendre(a_rendre, index_billet, caisse, billet_a_rendre):
    """Retourne le nombre de pièces ou billets
    de la valeur donnée à rendre en fonction 
    du nombre de pièces ou de billets de cette
    valeur dans la caisse
    """
    while a_rendre >= billet[index_billet] and caisse[index_billet] > 0:
            billet_a_rendre[index_billet] = billet_a_rendre[index_billet] + 1
            caisse[index_billet] -= 1
            a_rendre = a_rendre - billet[index_billet]
    return a_rendre 
    
caisse = [4, 5, 12, 4, 15]
billet = [50, 20, 10, 5, 1]
billet_a_rendre = [0,0,0,0,0]
# billet_a_rendre[0]  ->  nombre de billets de 50€
# billet_a_rendre[1]  ->  nombre de billets de 20€
# billet_a_rendre[2]  ->  nombre de billets de 10€
# billet_a_rendre[3]  ->  nombre de billets de 5€
# billet_a_rendre[4]  ->  nombre de billets de 1€

prix= 0
while prix <= 0 :
    prix = int(input("Prix à payer: "))        # prix à payer
   
recette = int(input("Montant déjà payé: "))   # montant reçu du client
while recette < prix :                          # vérifie que la somme donné est suffisante
       print ("Il manque", prix - recette,"euros")
       recette  = int(input("Payer le montant demandé:"))   # le client ressaisit une somme d'argent 

a_rendre = recette - prix       # calcul de la somme qu'on doit rendre au client (ce qu'il a payé - ce qu'il devait payer)

if a_rendre ==0 :            # si il n'y a rien à rendre, sortir de la boucle
       print ("\naucun rendu de monnaie\n")
else:   
    for index_billet in range(len(billet)):        
        a_rendre = rendre(a_rendre, index_billet, caisse, billet_a_rendre)
     # j'affiche le nombre de billets et de pièces de chaque valeur à rendre
    if a_rendre > 0:
        print("Désolé je n'ai pas la monnaie")
    else:
        for k in range(len(billet)): 
            print ("nb de billets de ", billet[k], "€  à rendre: ",billet_a_rendre[k])

