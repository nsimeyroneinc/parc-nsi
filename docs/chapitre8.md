---
title:  Chapitre 6  représentation des entiers
layout: parc
---





Cours de Pierre Duclosson et Frédéric Junier.



## Cours / TP

* [Cours / TP en version pdf](chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
* [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/2076-172112)
* [Corrigé du cours / TP en version pdf](chapitre8/Cours_Representation_Entiers_Correction.pdf)
* [Corrigé du cours en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a963-171873)


## Tutoriel vidéo 

??? video

    [Cours sur les opérations binaires de Pierre Marquestaut de l'académie de Bordeaux](https://peertube.lyceeconnecte.fr/videos/watch/b9913547-0bea-4d42-82ea-47598f2e74fd)

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.lyceeconnecte.fr/videos/embed/b9913547-0bea-4d42-82ea-47598f2e74fd" frameborder="0" allowfullscreen></iframe>
