---
title: Séance du 08/09/2021
---

# Séance du 08/09/2021

## Consignes et méthode de travail :

* Matériel :
    * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
    * Mettre le site <https://parc-nsi.github.io/premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site. Trois rubriques à retenir :
        * La [progression](https://parc-nsi.github.io/premiere/) avec les liens vers les chapitres
        * La liste des [séances](https://parc-nsi.github.io/premiere/seances/) détaillées qui sont ensuite copiées dans Pronote.
        * La rubrique [Automatismes](https://parc-nsi.github.io/premiere/automatismes/) avec des liens vers des QCM externes et des exercices pour travailler les automatismes.
    * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/ref/872118) n'est pas cher.
    * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

* Méthode de travail :
    * D'une séance à l'autre : relire le cours, faire les exercices
    * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
    * Évaluations :
        * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
        * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
        * Sommatives sous forme de devoir d'une heure ou de TP noté


## Chapitre 1: constructions de bases en langage Python


1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et commencer le chapitre 1 (affichage) du niveau 1.
2. Activité d'introduction pages 28 et 29 du manuel NSI Hachette. [Correction](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/exos/corrections/activitep28.py) dans l'environnement [Idle](https://www.python.org/downloads/) ou [Spyder (dans Anaconda)](https://www.anaconda.com/products/individual).



## A faire pour jeudi 09/09 :

1. Lire le [cours du chapitre 1](../chapitre1/cours/Chap1-Bases-Programmation-2021.pdf)
2. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et finir le chapitre 1 (affichage) du niveau 1.



![map](ressources/map.jpg){:align="center"}