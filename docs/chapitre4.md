---
title:  Chapitre 4  tableaux
layout: parc
---




Cours de Frédéric Junier.



# Cours

* [Cours en version pdf](chapitre6/Cours/tableaux-cours-.pdf)

* [Cours en version markdown](chapitre6/Cours/tableaux-cours-git.md)

* [Corrigé des exemples du cours en version html](chapitre6/Cours/ressources/PremiereNSI-Cours-Tableaux-Corriges.html)

* [Corrigé des exemples du cours en version pdf](chapitre6/Cours/ressources/PremiereNSI-Cours-Tableaux-Corriges.pdf)   

* [Corrigé des exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/68ac-101047)


* [Brouillon des exemples du cours](chapitre6/Cours/brouillon-prof-cours-tableaux.py)

    
# TP

* [TP en version pdf](chapitre6/TP/TP-tableaux-.pdf)

* [TP en version markdown](chapitre6/TP/TP-tableaux-git.md)
  
* [Matériel du TP à télécharger](chapitre6/TP/eleves.zip)

* [Corrigé du TP en version pdf](chapitre6/TP/corriges/TP-Tableaux-Corrige.pdf)
  
* [Corrigé du TP en version html](chapitre6/TP/corriges/TP-Tableaux-Corrige.html)
  
* [Corrigé  du TP en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/ba0b-101079)

# Tutoriel vidéo 

??? video

    [Cours sur les tableaux de Pierre Marquestaut de l'académie de Bordeaux](https://peertube.lyceeconnecte.fr/videos/watch/59073d01-ceff-4e4e-8387-012f79690eda)

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.lyceeconnecte.fr/videos/embed/59073d01-ceff-4e4e-8387-012f79690eda" frameborder="0" allowfullscreen></iframe>
