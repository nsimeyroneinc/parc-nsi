---
title:  Chapitre 1  Constructions de base d'un langage de programmation
---



Cours de Pierre Duclosson et Frédéric Junier.


## Cours

* [Pdf du cours](chapitre1/cours/Chap1-Bases-Programmation-2021.pdf)


## TP 

* TP1 :
    * [version Markdown](chapitre1/TP1/1NSI-Chap1-Variables-TP1-git.md)
    * [version PDF](chapitre1/TP1/1NSI-Chap1-Variables-TP1-.pdf)
    * [corrigé version PDF](chapitre1/TP1/corrigé-TP1.pdf)
    * [corrigé version ipynb](chapitre1/TP1/corrigé-TP1.ipynb)
* TP2 :
    * [énoncé PDF](chapitre1/TP2/TP2.pdf)
    * [corrigé pdf](chapitre1/TP2/correction/Correction_TP2.pdf)
    * [corrigé .ipynb](chapitre1/TP2/correction/Correction_TP2.ipynb)
    * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)

* QCM :
    * [QCM1 Genumsi](https://genumsi.inria.fr/qcm.php?h=e74b6446b2fb9380f06fe87ff3289bf4)

## Ressources


* Boucles `for` sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/Y-Ri2gdiOTA?list=PL2CXLryTKuwwlCGfgpApOTmMn7_dbtwWa" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* Tests et opérateurs booléens sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/0JXc48GXZrU?list=PL2CXLryTKuwwhivE1UO4Jg5DhU-ALAoXc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* Boucles `while` sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/gUZFAeM3VCc?list=PL2CXLryTKuwwhivE1UO4Jg5DhU-ALAoXc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




