---
title:  Automatismes
layout: parc
---

??? {{ automatisme() }}

    [Code Puzzle, fonction renvoyant le maximum d'un tableau](https://www.codepuzzle.io/p/Z72N)


??? {{ automatisme() }}

    [Code Puzzle, fonction testant l'appartenance d'un élément à un tableau](https://www.codepuzzle.io/p/TFZN)



??? {{ automatisme() }}

    Programmer la fonction dont on donne la spécification (référence [Upylab](https://upylab2.ulb.ac.be) :  CAIx-0300 chapitre tableaux)

    ~~~python
    def au_moins_un_zero(t):
        """
        Paramètre : t un tableau de nombres (int ou float)
        Précondition : t non vide
        Valeur renvoyée : un booléen indiquant si t contient au moins un zéro
        """
    ~~~


??? {{ automatisme() }}

    [Code Puzzle, 0 <-> 1](https://www.codepuzzle.io/p/WBGP)

    