# Solution 1

a = 842
b = 843
c = a   #variable de stockage temporaire
a = b
b = c


# Solution 2

a = 842
b = 843
a, b = b, a   #déballage de tuples

